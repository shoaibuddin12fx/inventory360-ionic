import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { NavService } from './services/basic/nav.service';
import { ModalService } from './services/basic/modal.service';
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions/ngx';
import { SqliteService } from './services/sqlite.service';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { SQLite } from '@ionic-native/sqlite/ngx';
import { StorageService } from './services/storage.service';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { SidemenuComponentModule } from './components/sidemenu/sidemenu.component.module';
import { NetworkService } from './services/network.service';
import { UsersService } from './services/users.service';
import { ApiService } from './services/api.service';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { InterceptorService } from './services/interceptor.service';
import { EventsService } from './services/basic/events.service';



@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule, 
    IonicModule.forRoot({
      mode: 'ios'
    }),
    AppRoutingModule,
    SidemenuComponentModule,
    HttpClientModule,
  
  ],
  providers: [
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    { provide: HTTP_INTERCEPTORS, useClass: InterceptorService, multi: true },
    NativePageTransitions,
    ModalService,
    NavService,
    SQLite,
    AndroidPermissions,
    StorageService,
    SqliteService,
    NativeStorage,
    NetworkService,
    UsersService,
    ApiService,
    EventsService


  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
