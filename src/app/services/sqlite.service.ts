import { Injectable } from '@angular/core';
import { SQLite, SQLiteDatabaseConfig, SQLiteObject } from '@ionic-native/sqlite/ngx';
import { Platform } from '@ionic/angular';
import { NetworkService } from './network.service';
import { UtilityService } from './utility.service';
import { StorageService } from './storage.service';
import { browserDBInstance } from './browser-db-instance';
import { CapacitorSQLite, JsonSQLite } from '@capacitor-community/sqlite';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { Capacitor } from '@capacitor/core';
import { resolve } from '@angular/compiler-cli/src/ngtsc/file_system';

const menu_json = require('../data/sidemenu.json');

declare var window: any;
const SQL_DB_NAME = '__inv360.db';

@Injectable({
  providedIn: 'root'
})
export class SqliteService {

  db: any;
  sqlite;
  DB_SETUP_KEY = 'first_db_setup';
  batchSqlCmd;
  batchSqlCmdVals;
  config: any = {
    name: '__inv360.db',
    location: 'default'
  }
  
  public msg = "Sync In Progress ...";

  constructor(
    private storage: StorageService, 
    private platform: Platform, 
    public androidpermissions: AndroidPermissions,    
    ) {

      


  }

  public initialize() {

    return new Promise(async resolve => {

      if (Capacitor.getPlatform() != 'web') {
        await this.initializeDatabase();
        resolve(true);
      } else {
        this.storage.getKey('is_database_initialized').then(async v => {
          if (!v) {
            await this.initializeDatabase();
            resolve(true);
          } else {
            resolve(true);
          }
        })
      }
      
      


      
    })

  }

  async initializeDatabase() {

    return new Promise(async resolve => {
      await this.platform.ready();
      // initialize database object
      const flag = await this.createDatabase();

      if (!flag) {
        resolve(false);
        return;
      }
      
      // initialize all tables

      // initialize users table
      await this.initializeUsersTable();

      await this.initializePermissionsTable();
      
      
      
      this.storage.setKey('is_database_initialized', true);
      resolve(true);
    })


  }

  async createDatabase() {
    return new Promise(async (resolve) => {
      if (Capacitor.getPlatform() != 'web') {
        var self = this;
          const dbName = SQL_DB_NAME;
          const dbNames = [SQL_DB_NAME];
          const ret = await CapacitorSQLite.checkConnectionsConsistency({dbNames:dbNames})
          if(ret.result == false){
            await CapacitorSQLite.createConnection({ database: dbName })
            await CapacitorSQLite.open({ database: dbName });
            resolve(true);
          }
          else{
            await CapacitorSQLite.open({ database: dbName });
            resolve(true);
        }
      } else {
        
        let _db = window.openDatabase(SQL_DB_NAME, '1.0', 'DEV', 5 * 1024 * 1024);
        this.db = browserDBInstance(_db);
        this.msg = 'Database initialized';
        resolve(true);
      }
    });
  }

  async initializeUsersTable() {

    return new Promise(resolve => {
      // create statement
      var sql = "CREATE TABLE IF NOT EXISTS users(";
      sql += "MembershipId TEXT PRIMARY KEY, ";
      sql += "EmployeeVendorId INTEGER, " 
      sql += "UserType INTEGER, " 
      sql += "UserDisplayImage VARCHAR, " 
      sql += "Email TEXT, " 
      sql += "FullName TEXT, " 
      sql += "storeName TEXT, " 
      sql += "StoreId INTEGER, " 
      sql += "StoreCode TEXT, " 
      sql += "GroupId INTEGER, " 
      sql += "GroupName TEXT, "
      sql += "active INTEGER DEFAULT 0, "
      sql += "token TEXT "
      sql += ")";

      this.msg = 'Initializing Users ...';
      resolve(this.execute(sql, []));
    })

  }


  // USERS QUERY STARTS

  public async setUserInDatabase(_user) {
    return new Promise(async resolve => {
      // check if user is already present in our local database, if not, create and fetch his data
      // check if user exist in database, if not create it else update it
      console.log(_user);
      var sql = "INSERT OR REPLACE INTO users(";
      sql += "MembershipId, ";
      sql += "EmployeeVendorId, " 
      sql += "UserType, " 
      sql += "UserDisplayImage, " 
      sql += "Email, " 
      sql += "FullName, " 
      sql += "storeName, " 
      sql += "StoreId, " 
      sql += "StoreCode, " 
      sql += "GroupId, " 
      sql += "GroupName " 
      sql += ") ";

      sql += "VALUES (";

      sql += "?, "
      sql += "?, " 
      sql += "?, " 
      sql += "?, " 
      sql += "?, " 
      sql += "?, " 
      sql += "?, " 
      sql += "?, " 
      sql += "?, " 
      sql += "?, " 
      sql += "? " // 11       
      sql += ")";

      var values = [
        _user.MembershipId,
        _user.EmployeeVendorId,
        _user.UserType,
        _user.UserDisplayImage,
        _user.Email,
        _user.FullName,
        _user.storeName,
        _user.StoreId,
        _user.StoreCode,
        _user.GroupId,
        _user.GroupName,
      ];

      await this.execute(sql, values);

      if (_user.MembershipId) {

        let sql3 = "UPDATE users SET active = ?";
        let values3 = [0];
        await this.execute(sql3, values3);

        let sql2 = "UPDATE users SET token = ?, active = ? where MembershipId = ?";
        let values2 = [_user.token, 1, _user.MembershipId];

        await this.execute(sql2, values2);

      }

      resolve(await this.getActiveUser());

    })
  }

  public async getCurrentUserAuthorizationToken() {
    return new Promise(async resolve => {
      let user_id = await this.getActiveUserId();
      let sql = "SELECT token FROM users where id = ? limit 1";
      let values = [user_id];

      let d = await this.execute(sql, values);
      // this.utility.presentToast(d);
      if (!d) {
        resolve(null);
        return;
      }
      // var data = d as any[];
      console.log("d", d);
      const data = this.getRows(d);
      if (data.length > 0) {
        resolve(data[0]['token']);
      } else {
        resolve(null);
      }

    })
  }

  public async setUserActiveById(id) {

    return new Promise(async resolve => {

      let sql3 = "UPDATE users SET active = ?";
      let values3 = [0];
      await this.execute(sql3, values3);

      let sql2 = "UPDATE users SET active = ? where id = ?";
      let values2 = [ 1, id];
      await this.execute(sql2, values2);

      resolve(this.getUserById(id))

    })

  }

  public async setRefreshToken(token, refreshToke) {

    return new Promise(async resolve => {

      let sql3 = "UPDATE users SET token = ?, refreshToken = ? where active = ?";
      let values3 = [token, refreshToke, 1];
      await this.execute(sql3, values3);
      
      resolve(true)

    })

  }

  public async getUserById(id) {

    return new Promise(async resolve => {
      let sql = "SELECT * FROM users where id = ?";
      let values = [id];

      let d = await this.execute(sql, values);
      if (!d) {
        resolve(null);
      }
      // var data = d as any[];
      const data = this.getRows(d);
      if (data.length > 0) {
        let id = data[0];
        resolve(id);
      } else {
        resolve(null);
      }

    })

  }

  public async getActiveUserId() {

    return new Promise(async resolve => {
      let sql = "SELECT MembershipId FROM users where active = ?";
      let values = [1];

      let d = await this.execute(sql, values);
      if (!d) {
        resolve(null);
      }
      // var data = d as any[];
      console.log(d);
      const data = this.getRows(d);
      if (data.length > 0) {
        let id = data[0]["MembershipId"];
        resolve(id);
      } else {
        resolve(null);
      }

    })

  }

  public async getActiveUser() {
    return new Promise(async resolve => {
      let sql = "SELECT * FROM users where active = ? ";
      let values = [1];

      let d = await this.execute(sql, values);
      var _data = d as any[];
      const data = this.getRows(_data);
      if (data && data.length > 0) { 
        var user = data[0];
        console.log(user)
        resolve(user);
      } else {
        resolve(null);
      }

    })
  }

  setLogout() {

    return new Promise(async resolve => {
      let user_id = await this.getActiveUserId();

      let sql = "UPDATE users SET token = ?, active = ? where MembershipId = ?";
      let values = [null, 0, user_id];

      let d = await this.execute(sql, values);
      // var data = d as any[];
      const data = this.getRows(d);
      if (data.length > 0) {
        resolve(true);
      } else {
        resolve(false);
      }


    })

  }



  // Permissions CRUD STARTS

  async initializePermissionsTable() {

    return new Promise(resolve => {
      // create statement
      var sql = "CREATE TABLE IF NOT EXISTS permissions(";
      sql += "MembershipId TEXT, ";
      sql += "PermissionName TEXT "
      sql += ")";

      this.msg = 'Initializing permissions ...';
      resolve(this.execute(sql, []));
    })

  }

  public async setPermissionsInDatabase(_permissions) {


    let mid = await this.getActiveUserId();

    if(!mid){
      resolve(null);
      return;
    }

    await this.deleteAllPermissions(mid);

    return new Promise(async resolve => {
      // check if user is already present in our local database, if not, create and fetch his data
      // check if user exist in database, if not create it else update it
      console.log(_permissions);

      for(var i = 0; i < _permissions.length; i++){

        var sql = "INSERT OR REPLACE INTO permissions(";
        sql += "MembershipId, ";
        sql += "PermissionName "         
        sql += ") ";

        sql += "VALUES (";

        sql += "?, "
        sql += "? " 
        sql += ")";

        var values = [
          mid,
          _permissions[i].EmployeeVendorId
        ];

        await this.execute(sql, values);

      }

      resolve(true);

    })
  }

  deleteAllPermissions(mid) {

    return new Promise(async resolve => {
      
      let sql = "DELETE from permissions where MembershipId = ?";
      let values = [mid];

      let d = await this.execute(sql, values);
      // var data = d as any[];
      const data = this.getRows(d);
      if (data.length > 0) {
        resolve(true);
      } else {
        resolve(false);
      }


    })

  }

  


  // Permissions CRUD END



  execute(sql, params) {
    return new Promise(async resolve => {

      if (!this.db) {
        await this.platform.ready();
        // initialize database object
        await this.createDatabase()
      }

      console.log(sql);
      // // if(this.platform.is('cordova')){
      console.log(params);

      if( Capacitor.getPlatform() != 'web' ){

        return CapacitorSQLite.query({database:SQL_DB_NAME,statement:sql,values:params})
        .then((response) => {
          console.log(response);
          resolve(response);
        })
        .catch((err) => {
          console.error(err);
          resolve(null);
          
        });

      }else{
        this.db.executeSql(sql, params).then(response => {
          resolve(response);
        }).catch(err => {
          console.error(err);
          resolve(null);
        })
      }

      
      
    })
  }

  prepareBatch(insertRows){

    return new Promise( async resolve => {

      var size = 250; var arrayOfArrays = [];
      
      for (var i=0; i<insertRows.length; i+=size) {
        arrayOfArrays.push(insertRows.slice(i,i+size));
      }

      console.log(arrayOfArrays);

      for(var j = 0; j < arrayOfArrays.length; j++){
        await this.executeBatch(arrayOfArrays[j]);
        // await this.execute(s, p)
      }

      resolve(true);

    })
  }

  executeBatch(array) {

    return new Promise(async resolve => {

      if (!this.db) {
        await this.platform.ready();
        // initialize database object
        await this.createDatabase()
      }

      console.log(array); 
      this.db.sqlBatch(array).then(response => {
        resolve(response);
      }).catch(err => {
        console.error(err);
        resolve(null);
      })
      
    })
  }


  private setValue(k, v) {
    return new Promise(resolve => {
      this.storage.setKey(k, v).then(() => {
        resolve({ k: v });
      });
    })

  }

  private getValue(k): Promise<any> {
    return new Promise(resolve => {
      this.storage.getKey(k).then(r => {
        resolve(r);
      });
    })

  }

  private getRows(data) {
    var items = [];
    console.log('data here',{data});
    for (let i = 0; i < data.values.length; i++) {
      let item = data.values[i];

      items.push(item);
    }

    console.log("items here: ",items);
    return items;
  }




}
