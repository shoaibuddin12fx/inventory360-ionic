import { Injectable } from '@angular/core';
import { NavService } from './basic/nav.service';
import { NetworkService } from './network.service';
import { SqliteService } from './sqlite.service';

@Injectable({
  providedIn: 'root'
})
export class UsersService {


  constructor(public sqlite: SqliteService, public network: NetworkService, public nav: NavService) { }

  getActiveUser(){
    return this.sqlite.getActiveUser();
  }

  login(formdata){
    return new Promise( async resolve => {
      const res = await this.network.login(formdata);
      if(res.length > 0){
        const user = await this.sqlite.setUserInDatabase(res[0]);

        // get user rights and save in database
        const res2 = await this.network.getRights();
        console.log(res2);









        
        resolve(user);
      } else {
        resolve(null);
      }
    })
    
    
    
  }

  getUserToken(){
    return 'ABCD';
  }

  logout(){
    // this.menuCtrl.enable(false, 'authenticated');
    this.sqlite.setLogout();
    this.nav.setRoot('pages/login');
  }

}
