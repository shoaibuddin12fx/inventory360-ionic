import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import { Capacitor } from '@capacitor/core';
import {Config} from '../config/main.config';
import { Http, HttpResponse } from '@capacitor-community/http';

// https://github.com/capacitor-community/http



@Injectable({
    providedIn: 'root'
})
export class ApiService {

    constructor(public http: HttpClient) {
    }

    async get(endpoint: string, params?: any, reqOpts?: any, fromCRM = false) {
        let serviceUrl = fromCRM ? Config.CRM_SERVICEURL : Config.SERVICEURL;

        // check for platform 
        //if( Capacitor.getPlatform() != 'web' ){

            const options = {
                url: serviceUrl + '/' + endpoint,
                params: params,
            };
        
            return await Http.get(options);
        // }else{
        //     return this.http.get(serviceUrl + '/' + endpoint, reqOpts);
        // }
        
    }

    async post(endpoint: string, body: any, reqOpts?: any, fromCRM = false) {
        let serviceUrl = fromCRM ? Config.CRM_SERVICEURL : Config.SERVICEURL;

        // if( Capacitor.getPlatform() != 'web' ){
            const options = {
                url: serviceUrl + '/' + endpoint,
                data: body,
            };
        
            return await Http.post(options);
        // } else {
        //     return this.http.post(serviceUrl + '/' + endpoint, body, reqOpts);
        // }
        
    }

    put(endpoint: string, body: any, reqOpts?: any, fromCRM = false) {
        let serviceUrl = fromCRM ? Config.CRM_SERVICEURL : Config.SERVICEURL;
        return this.http.put(serviceUrl + '/' + endpoint, body, reqOpts);
    }

    delete(endpoint: string, reqOpts?: any, fromCRM = false) {
        let serviceUrl = fromCRM ? Config.CRM_SERVICEURL : Config.SERVICEURL;
        return this.http.delete(serviceUrl + '/' + endpoint, reqOpts);
    }

    patch(endpoint: string, body: any, reqOpts?: any, fromCRM = false) {
        let serviceUrl = fromCRM ? Config.CRM_SERVICEURL : Config.SERVICEURL;
        return this.http.patch(serviceUrl + '/' + endpoint, body, reqOpts);
    }
}
