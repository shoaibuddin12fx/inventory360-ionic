import { HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Capacitor } from '@capacitor/core';
import { ApiService } from './api.service';
import { SqliteService } from './sqlite.service';
import { UtilityService } from './utility.service';

@Injectable({
  providedIn: 'root'
})
export class NetworkService {
  
  ongoing = false;

	constructor(
		public utility: UtilityService,
		public api: ApiService,
		public sqlite: SqliteService
	) {

  }

  login(params){
    const str = this.serialize(params);
    return this.httpPostResponse('Login' + '?' + str, null, false, true);    
  }

  async checkVin(params){

	// added params
	let mid = await this.sqlite.getActiveUserId();
	params['membershipId'] = mid;
	params['vinnumber'] = params['vinnumber'];
    params['storeId'] = 13;
	params['IsAddNow'] = params['IsAddNow'] ?? false;

    const str = this.serialize(params);
    return this.httpGetResponse('GetBasicTabDataFromVinWithDDL' + '?' + str, null, false, true);    
  }


  async getRights(params = {}){

	// added params
	let mid = await this.sqlite.getActiveUserId();
	params['membershipId'] = mid;
	
    const str = this.serialize(params);
    return this.httpGetResponse('GetRights' + '?' + str, null, false, true);    
  }
  
  serialize = ((obj) => {
    const str = [];
    for (const p in obj) {
      if (obj.hasOwnProperty(p)) {
        str.push(encodeURIComponent(p) + '=' + encodeURIComponent(obj[p]));
      }
    }
    return str.join('&');
  });


  httpPostResponse(key, data, id = null, showloader = false, showError = true, contenttype = 'application/json') {
		return this.httpResponse('post', key, data, id, showloader, showError, contenttype);
	}

	httpGetResponse(key, id = null, showloader = false, showError = true, contenttype = 'application/json') {

		return this.httpResponse('get', key, {}, id, showloader, showError, contenttype);
	}

	// default 'Content-Type': 'application/json',
	httpResponse(type = 'get', key, data, id = null, showloader = false, showError = true, contenttype = 'application/json'): Promise<any> {

		return new Promise(async (resolve, reject) => {
			this.ongoing = true;

			// tslint:disable-next-line:triple-equals
			if (showloader == true) {
				await this.utility.showLoader();
			}

			// tslint:disable-next-line:variable-name
			const _id = (id) ? '/' + id : '';
			const url = key + _id;


			console.log(contenttype);
			const reqOpts = {
				headers: new HttpHeaders({
					'Content-Type': contenttype,
				})
			};

			// tslint:disable-next-line:triple-equals
			const seq = (type == 'get') ? this.api.get(url, null, reqOpts) : this.api.post(url, data, reqOpts);

			seq.then((resObj: any) => {
				this.ongoing = false;
				// tslint:disable-next-line:triple-equals
				if (showloader == true) {
					this.utility.hideLoader();
				}
				console.log(resObj);
				let _data = resObj.data;


				if(_data.Status == 1){
                        
					if(_data.Data){
						console.log(resObj.data.Data[0])
						resolve(resObj.data.Data);
					}else{						
						resolve([]);
					}
					
				}
				else {			
					
					// show message 
					this.utility.presentFailureToast(_data.Message)
					reject(null);					
				}





				reject(null);

				// this.utility.presentSuccessToast(res['message']);

			}, err => {
				this.ongoing = false;

				console.log({err});

				const error = err?.error;
				// tslint:disable-next-line:triple-equals
				if (showloader == true) {
					this.utility.hideLoader();
				}

				if (showError && err.status != 401) {
					this.utility.presentFailureToast(error?.message);
				}

				reject(null);

			});

		});

	}

	showFailure(err) {
		// console.error('ERROR', err);
		// tslint:disable-next-line:variable-name
		const _error = (err) ? err?.message : 'check logs';
		this.utility.presentFailureToast(_error);
	}
}
