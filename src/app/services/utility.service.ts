import { Injectable } from '@angular/core';
import { Capacitor } from '@capacitor/core';
import { AlertsService } from './basic/alerts.service';
import { LoadingService } from './basic/loading.service';
@Injectable({
  providedIn: 'root'
})
export class UtilityService {

  constructor(
    public loading: LoadingService,
    public alerts: AlertsService,
    ) { }

  isWeb(){
    return Capacitor.getPlatform() == 'web';
  }

  showLoader(msg = 'Please wait...') {
    return this.loading.showLoader(msg);
  }

  hideLoader() {
    return this.loading.hideLoader();
  }

  showAlert(msg) {
    return this.alerts.showAlert(msg);
  }

  presentToast(msg) {
    return this.alerts.presentToast(msg);
  }

  presentSuccessToast(msg) {
    return this.alerts.presentSuccessToast(msg);
  }

  presentFailureToast(msg) {
    return this.alerts.presentFailureToast(msg);
  }

  presentConfirm(okText = 'OK', cancelText = 'Cancel', title = 'Are You Sure?', message = ''): Promise<boolean> {
    return this.alerts.presentConfirm(okText = okText, cancelText = cancelText, title = title, message = message);
  }


}
