import { Injectable } from '@angular/core';
import { UtilityService } from './utility.service';
import { BarcodeScanner } from '@capacitor-community/barcode-scanner';
import { NetworkService } from './network.service';

@Injectable({
  providedIn: 'root'
})
export class ScanService {

  
  constructor(private utility: UtilityService, private network: NetworkService) { 

  }


  scanCode(){

    return new Promise( async resolve => {
      let code = await this.hitScan();
      if(code){
  
        // 
        this.utility.presentSuccessToast("api will be called");
        resolve(true);
  
      }else {
        this.utility.presentFailureToast("Invaild Vin")
        resolve(false);
      }
    })

    

  }

  hitScan(){

    return new Promise( async resolve => {

      const flag = await didUserGrantPermission()

      if(flag){
        BarcodeScanner.hideBackground(); // make background of WebView transparent
        document.body.style.opacity = '0';
        const result = await BarcodeScanner.startScan(); // start scanning and wait for a result
        document.body.style.opacity = '1';
        // if the result has content

        console.log("log result", result);


        if (result.hasContent) {
          console.log(result.content); // log the raw scanned content

          this.network.checkVin({
            vinnumber: result.content,
            IsAddNow: false
          })




        }else{
          resolve(null)
        }





      }else{
        resolve(null)
      }

      

      
    });

  }

  stopScan(){

  }



}

const didUserGrantPermission = async () => {
  // check if user already granted permission
  const status = await BarcodeScanner.checkPermission({ force: false });

  if (status.granted) {
    // user granted permission
    return true;
  }

  if (status.denied) {
    // user denied permission
    return false;
  }

  if (status.asked) {
    // system requested the user for permission during this call
    // only possible when force set to true
  }

  if (status.neverAsked) {
    // user has not been requested this permission before
    // it is advised to show the user some sort of prompt
    // this way you will not waste your only chance to ask for the permission
    const c = confirm(
      'We need your permission to use your camera to be able to scan barcodes',
    );
    if (!c) {
      return false;
    }
  }

  if (status.restricted || status.unknown) {
    // ios only
    // probably means the permission has been denied
    return false;
  }

  // user has not denied permission
  // but the user also has not yet granted the permission
  // so request it
  const statusRequest = await BarcodeScanner.checkPermission({ force: true });

  if (statusRequest.asked) {
    // system requested the user for permission during this call
    // only possible when force set to true
  }

  if (statusRequest.granted) {
    // the user did grant the permission now
    return true;
  }

  // user did not grant the permission, so he must have declined the request
  return false;
};
