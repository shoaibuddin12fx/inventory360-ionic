import { Component, Injector, OnInit } from '@angular/core';
import { BasePage } from 'src/app/pages/base-page/base-page';


const menuItems = require('src/app/data/sidemenu.json');

@Component({
  selector: 'app-sidemenu',
  templateUrl: './sidemenu.component.html',
  styleUrls: ['./sidemenu.component.scss'],
})
export class SidemenuComponent extends BasePage implements OnInit {

  logo_image = '';
  // public appPages = [
  //   { title: 'Audit', url: '/login', icon: 'dashboard' },
  //   { title: 'Inventory', url: '/login', icon: 'paper-plane' },
  //   { title: 'Detailing', url: '/login', icon: 'data_usage' },
  //   { title: 'Manage Ro', url: '/login', icon: 'archive' },
  //   { title: 'Check In/Out', url: '/login', icon: 'sync' },
  //   { title: 'We Owe', url: '/login', icon: 'warning' },
  //   { title: 'CR Report', url: '/login', icon: 'warning' },
  //   { title: 'Recent Vehicles', url: '/login', icon: 'warning' },
  //   { title: 'Dashboard', url: '/login', icon: 'dashboard' },
  //   { title: 'Logout', url: '/login', icon: 'arrow_forward' },
  // ];

  collectionArray = [];
  constructor(injector: Injector) { 
    super(injector);
    this.collectionArray = menuItems;
  }

  ngOnInit() {}

  logout(){
    setTimeout( () => {
      this.users.logout()
    }, 1000)
    
  }

}
