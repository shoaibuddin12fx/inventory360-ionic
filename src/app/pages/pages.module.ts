import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { IonicModule } from "@ionic/angular";
import { PagesRoutingModule } from "./ pages-routing.module";

@NgModule({
    declarations: [],
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        IonicModule,
        PagesRoutingModule,
    ],
    exports: [

    ],
    providers: [
        Location,
    ]

})
export class PagesModule{}