import { Component, Injector, OnInit } from '@angular/core';
import {FormGroup, Validators} from '@angular/forms';
import {BasePage} from '../base-page/base-page';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})

export class LoginPage extends BasePage implements OnInit {

  aForm: FormGroup;

  constructor(injector: Injector) {
    super(injector);
    this.setupForm();
   }

  ngOnInit() {
  }

  setupForm() {

		this.aForm = this.formBuilder.group({
			username: ['j.malik2694', Validators.compose([Validators.required]) /*, VemailValidator.checkEmail */],
			password: ['alpha442a', Validators.compose([Validators.required])]
		});

	}

  async login() {
    const formdata = this.aForm.value;
    console.log('formdata', formdata);

    if(this.aForm.invalid){
      this.utility.presentFailureToast("Username and password required");
      return;
    }

    const res = await this.users.login(formdata);
    if(res){
      this.nav.push('pages/dashboard');
    }
  }

}
