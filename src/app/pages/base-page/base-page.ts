import { Injector } from '@angular/core';
import { Location } from '@angular/common';
import { Platform, MenuController } from '@ionic/angular';
import { FormBuilder } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { ModalService } from 'src/app/services/basic/modal.service';
import { NavService } from 'src/app/services/basic/nav.service';
import { ScanService } from 'src/app/services/scan.service';
import { SqliteService } from 'src/app/services/sqlite.service';
import { UsersService } from 'src/app/services/users.service';
import { UtilityService } from 'src/app/services/utility.service';
import { EventsService } from 'src/app/services/basic/events.service';

export abstract class BasePage {

    public platform: Platform;
    public formBuilder: FormBuilder;
    public menuCtrl: MenuController;
    public domSanitizer: DomSanitizer;
    public modals: ModalService;
    public nav: NavService;
    public scan: ScanService;
    public sqlite: SqliteService;
    public users: UsersService;
    public utility: UtilityService;
    public events: EventsService;
    
    constructor(injector: Injector) {
        this.platform = injector.get(Platform);
        this.formBuilder = injector.get(FormBuilder);
        this.menuCtrl = injector.get(MenuController);
        this.domSanitizer = injector.get(DomSanitizer);
        this.nav = injector.get(NavService);
        this.modals = injector.get(ModalService);
        this.scan = injector.get(ScanService);
        this.sqlite = injector.get(SqliteService);
        this.users = injector.get(UsersService);
        this.utility = injector.get(UtilityService);
        this.events = injector.get(EventsService);
    }


}
