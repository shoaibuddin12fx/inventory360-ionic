import { Component, Injector, OnInit } from '@angular/core';
import { BasePage } from '../base-page/base-page';
const menuItems = require('src/app/data/sidemenu.json');

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage extends BasePage implements OnInit {

  collectionArray = [];
  constructor(injector: Injector) { 
    super(injector);
    this.collectionArray = menuItems;
  }

  ngOnInit() {
  }

  scanBarcode(){

    // open qrcode view
    this.scan.scanCode();
  }

}
