import { Component, Injector, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-splash',
  templateUrl: './splash.page.html',
  styleUrls: ['./splash.page.scss'],
})
export class SplashPage extends BasePage implements OnInit {

  constructor(injector: Injector,  private menu: MenuController) { 
    super(injector);
    this.initialize();
    
  }

  ngOnInit() {

  }

  async initialize(){

    await this.platform.ready()
    // this.splashScreen.hide();
    this.menu.swipeGesture(false);
    this.nav.push('pages/login');
    await this.sqlite.initialize();
    const user = await this.users.getActiveUser();
    if(!user){
      this.nav.push('pages/login');
    }else{
      this.nav.push('pages/dashboard');
    }

  }



}
