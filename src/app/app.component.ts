import { Component } from '@angular/core';
import { Platform } from '@ionic/angular';
import { StatusBar, Style } from '@capacitor/status-bar';
import { UtilityService } from './services/utility.service';
import { SqliteService } from './services/sqlite.service';
import { NavService } from './services/basic/nav.service';


@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  
  public labels = ['Family', 'Friends', 'Notes', 'Work', 'Travel', 'Reminders'];
  constructor(
    private platform: Platform,
    private utility: UtilityService,
    private sqlite: SqliteService,
    private nav: NavService
  ) {

    this.initializeApp();

  }

  async initializeApp() {
    this.platform.ready().then( async () => {
      
      if(!this.utility.isWeb){
        StatusBar.setOverlaysWebView({ overlay: true });
        StatusBar.setBackgroundColor({
          color: '#00a185'
        })
      }
      
    });
  }
}
