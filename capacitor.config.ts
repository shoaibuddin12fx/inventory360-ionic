import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'com.pixeltech.inventory360',
  appName: 'Inventory360',
  webDir: 'www',
  bundledWebRuntime: false
};

export default config;
